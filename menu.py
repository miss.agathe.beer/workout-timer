import pygame
from pygame.locals import *
import area
    
class myMenu :
    """ this class contains menus appearing during the game """
    
    def __init__(self, window, buttons = [], title = "") :
        self.window = window
        self.buttons = buttons
        
    def main_menu(self) :
        w, h = self.window.get_size()
        pygame.display.flip()
        mousePos = pygame.mouse.get_pos()

        # Buttons
        start_button = area.Area(100, 50, 10, 357, (251, 204, 209), 'Start')
        pause_button =  area.Area(100, 50, 120, 357, (251, 204, 209), 'Pause')
        reset_button = area.Area(100, 50, 516, 357, (251, 204, 209), 'Reset')

        self.buttons = [start_button, pause_button, reset_button]

        # background
        background = pygame.image.load("wallpaper.jpg").convert()

        self.window.blit(background, (0, 0))
    
        start_button.draw(self.window, (0, 0, 0), 30)
        pause_button.draw(self.window, (0, 0, 0), 30) 
        reset_button.draw(self.window, (0, 0, 0), 30)    
    
    