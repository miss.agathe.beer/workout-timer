import pygame
import menu
import event
import timer
import argparse


class myWorkout(event.EventClass):

    def __init__(self, work, rest, rounds):
        self.work = work # work duration
        self.rest = rest # rest duration
        self.rounds = rounds # number of rounds
        self.running = True
        self.display_surf = None
        self._image_surf = None
        width = 626
        height = 417
        self.width = width 
        self.height = height
        self.size = width, height
        self.current_round = 0
        self.left = 0
        self.initialized = 1
        self.timer_is_running = 0
        self.blit_list = []

    def on_init(self):
        # super().on_init()
        pygame.init()
        pygame.display.set_caption('My Workout')
        self.display_surf = pygame.display.set_mode(
            self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self._image_surf = pygame.image.load("wallpaper.jpg").convert()
        self.running = True
        self.screen = menu.myMenu(self._image_surf)
        self.screen.main_menu()
        self.myTimer = timer.Timer(self._image_surf, self.initialized, self.rounds, self.work, self.rest)
        self.myTimer.timer_initialization()

    def on_exit(self):
        self.running = False

    def on_loop(self):
        self.myTimer.timer_is_running()
        self.myTimer.timer_display()

    def mouse_button_down(self, event):
        mousePos = pygame.mouse.get_pos() 

        if self.screen.buttons[0].isOver(mousePos): 
            self.myTimer.go() 
            self.initialized = 0
        elif self.screen.buttons[1].isOver(mousePos):
            self.myTimer.pause()
        elif self.screen.buttons[2].isOver(mousePos):
            self.myTimer.pause()
            self.initialized = 1
            self.myTimer.timer_initialization()
            
        

    def on_render(self):
        self.display_surf.blit(self._image_surf, (0, 0))
        pygame.display.flip()

    def on_cleanup(self):
        pygame.quit()

    def on_execute(self):

        if self.on_init() is False:
            self.running = False

        while(self.running):
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
        self.on_cleanup()


if __name__ == "__main__": # definition point entree 
    parser = argparse.ArgumentParser()

    parser.add_argument("--work", help="effort duration in seconds", type=int, default=40)
    parser.add_argument("--rest", help="rest duration in seconds", type=int, default=20)
    parser.add_argument("--rounds", help="number of rounds", type=int, default=30)

    args = parser.parse_args()
    workout = myWorkout(args.work, args.rest, args.rounds)
    workout.on_execute()

    ####### Line to type for 30 rounds, 45 sec exercise, 15 sec rest : 
    # python workout.py --work=45 --rest=15 --rounds=20
    #######

    ####### Line to type to get some help : 
    # python workout.py -h
    #######
    
