import pygame 
from pygame import mixer
import time 
import area

class Timer: 
    def __init__(self, window, initialized, rounds, work, rest): 
        self.window = window
        self.initialized = initialized
        if self.initialized == 1 : 
            self.mode = "rest" # rest or work
        if self.initialized == 0 : 
            self.mode = "work"
        self.rounds = rounds
        self.work = work 
        self.rest = rest 
        self.current_round = None 
        self.left = None
        self.run = 0
        pygame.mixer.init()
        self.alarm_sound = pygame.mixer.Sound("dr_maboul.wav")

    
    
    def timer_initialization(self): 
        self.mode = "rest"
        self.left = self.rest
        self.current_round = 1
        self.run = 0

    def alarm(self): 
        # import pdb; pdb.set_trace()
        self.alarm_sound.play()

    def timer_is_running(self): 
        if self.current_round <= self.rounds and self.run == 1: 
            if self.mode == "rest": 
                if self.left==0: 
                    self.mode = "work"
                    self.left = self.work+1
                elif self.left <=3 : 
                    self.alarm()
                    time.sleep(1)
                    self.left-=1 
                else:
                    time.sleep(1)
                    self.left-=1

            
            if self.mode == "work": 
                if self.left==0: 
                    self.mode = "rest"
                    self.left = self.rest
                    self.current_round+=1 
                elif self.left <= 3 :
                    self.alarm() 
                    time.sleep(1)
                    self.left-=1 
                else:
                    time.sleep(1)
                    self.left-=1
        else:
            self.timer_initialization()


            
    
    def timer_display(self):
        w, h = self.window.get_size()
        pygame.display.flip()
        mousePos = pygame.mouse.get_pos()

        round_display = area.Area(150, 80, 0, 0, (251, 204, 209), str(self.current_round))
        time_display = area.Area(226, 117, 200, 150, (251, 204, 209), str(self.left))

        if self.mode == "work": 
            round_display.draw(self.window, (255, 0, 0), 40)
            time_display.draw(self.window, (255, 0, 0), 80)
            
        if self.mode == "rest": 
            round_display.draw(self.window, (0, 255, 0), 40)
            time_display.draw(self.window, (0, 255, 0), 80)

    def pause(self):
        self.run=0

    def go(self):
        self.run=1

        
        

    